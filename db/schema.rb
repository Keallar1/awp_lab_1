# frozen_string_literal: true

require 'sequel'

module Auth
  Auth.db.create_table? :users do
    primary_key :id
    String :name, unique: true, null: false
    String :role, null: false, default: 'user'
    String :login, unique: true, null: false
    String :password, null: false
    String :token, unque: true, null: false
    Boolean :block, null: false, default: false
    Boolean :limitation, null: false, default: false
    DateTime :created_at, null: false, default: Sequel.lit('DATE(\'now\')')
  end
end
