# frozen_string_literal: true

require_relative '../config/application'
require 'bcrypt'

if Auth.db[:users].count == 0
  Auth.db[:users].insert(name: 'TestUser',
                         role: 'user',
                         login: 'test@test.ru',
                         password: BCrypt::Password.create('12345').to_s,
                         token: SecureRandom.hex(10))

  Auth.db[:users].insert(name: 'AdminUser',
                         role: 'admin',
                         login: 'admin@admin.ru',
                         password: BCrypt::Password.create('qwerty$4').to_s,
                         token: SecureRandom.hex(10))
end
