# frozen_string_literal: true

module Auth
  module Authentication
    def logged_in?
      !!session[:token]
    end

    def current_user
      Auth.db[:users].where(token: session[:token]) if session[:token]
    end

    def hash_password(password)
      BCrypt::Password.create(password).to_s
    end

    def check_password(password, hash)
      BCrypt::Password.new(hash) == password
    end

    def change_password(user, old_password, new_password)
      return unless check_password(old_password, current_user.first[:password])

      changed_password = hash_password(new_password)
      user.update(password: changed_password)
    end

    def redirect_if_not_logged_in
      redirect to '/sign_in' if !logged_in? || current_user.first[:block]
    end

    def admin?
      current_user.first[:role] == 'admin' if current_user
    end

    def limited?
      current_user.first[:limitation]
    end

    def check_limitation_pass(password)
      return true if password.match(/^(?=.*[a-zA-Z])(?=.*\d)(?=.*[\+\-\*\/]).*$/)

      false
    end
  end
end
