# frozen_string_literal: true

module Auth
  module Crypto
    # rubocop:disable Metrics/MethodLength
    def encrypt(output_file, key, input_file = 'db/development.sqlite3')
      FileUtils.mv input_file, "#{input_file}.txt"
      ext_input_file = "#{input_file}.txt"
      output_file = "db/#{output_file}"
      return if File.exist?(output_file)

      key = Digest::SHA256.digest(key)
      random_value = OpenSSL::Random.random_bytes(32)
      key_with_random = key + random_value

      cipher = OpenSSL::Cipher.new('aes-256-ecb')
      cipher.encrypt
      cipher.key = key

      # return unless File.exist?(input_file)

      File.open(ext_input_file, 'rb') do |input|
        File.open(output_file, 'wb') do |output|
          while (block = input.read(cipher.block_size))
            encrypted_block = cipher.update(block)
            output.write(encrypted_block)
          end
          output.write(cipher.final)
        end
      end
      File.delete(ext_input_file)
    rescue Errno::ENOENT
      puts 'not exist'
    end

    def decrypt(input_file, key, output_file = 'db/development.sqlite3')
      cipher = OpenSSL::Cipher.new('aes-256-ecb')
      cipher.decrypt
      cipher.key = Digest::SHA256.digest(key)

      File.open("db/#{input_file}", 'rb') do |input|
        encrypted_data = input.read
        decrypted_data = cipher.update(encrypted_data) + cipher.final
        File.open(output_file, 'wb') do |output|
          while (block = input.read(cipher.block_size))
            decrypted_block = cipher.update(block)
            output.write(decrypted_block)
          end
          output.write(cipher.final)
        end
      end
    rescue StandardError => e
      puts e.message
      puts 'not exist'
    ensure
      session[:cypher] = 1
      connect_db
      File.delete("db/#{input_file}") if File.exist?("db/#{input_file}")
    end

    def connect_db
      Auth.db = Sequel.connect(DB_CONFIG['development'])
      puts Auth.db
      # Auth.db.drop_table? :users
      load_schema
      load('db/seeds.rb')
    end

    def load_schema
      Auth.db.create_table? :users do
        primary_key :id
        String :name, unique: true, null: false
        String :role, null: false, default: 'user'
        String :login, unique: true, null: false
        String :password, null: false
        String :token, unque: true, null: false
        Boolean :block, null: false, default: false
        Boolean :limitation, null: false, default: false
        DateTime :created_at, null: false, default: Sequel.lit('DATE(\'now\')')
      end
    end
    # rubocop:enable Metrics/MethodLength
  end
end
