# frozen_string_literal: true

module Auth
  module Authorization
    def check_session_block
      exit if session[:block] > 3
    end

    def blocked?(user)
      redirect to '/blocked' if user[:block]
    end
  end
end
