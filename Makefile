start:
	docker-compose up -d

stop:
	docker-compose stop

down:
	docker-compose down

bash:
	docker-compose run --rm app bash

console:
	docker-compose run --rm app rake console