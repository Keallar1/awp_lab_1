# frozen_string_literal: true

require 'sequel'
require 'yaml'
require 'bcrypt'
require 'openssl'
require 'rake'
require 'digest/sha2'
require 'securerandom'
require 'fileutils'

module Auth
  @db = ''
  DB_CONFIG = YAML.load_file('config/database.yml')
  # DB = Sequel.connect(DB_CONFIG['development'])
  KEY = 'password'
  require './helpers/authentication'
  require './helpers/authorization'
  require './helpers/crypto'

  def self.db
    @db
  end

  def self.db=(val)
    @db = val
  end
end
