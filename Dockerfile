FROM ruby:3.1.2
ENV APP_PATH=/app
WORKDIR $APP_PATH

ADD Gemfile $APP_PATH/Gemfile
ADD Gemfile.lock $APP_PATH/Gemfile.lock

RUN bundle install

ADD . /app