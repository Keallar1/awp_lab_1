# frozen_string_literal: true

require 'rack/unreloader'
Unreloader = Rack::Unreloader.new{Auth::App}
Unreloader.require './app.rb'

run Unreloader
