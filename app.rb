# frozen_string_literal: true

require 'sinatra'
require 'sinatra/namespace'
require './config/application'

module Auth
  # rubocop:disable Metrics/BlockLength
  # rubocop:disable Metrics/ClassLength
  class App < Sinatra::Base
    register Sinatra::Namespace

    configure do
      set public_folder: 'public'
      set content_type: :json
      set views: 'views'
      set :json_encoder, :to_json
      set sessions: true
    end

    helpers Authentication, Authorization, Crypto

    before do
      pass if request.path_info.include?('access') || session[:token]

      redirect to '/access' if session[:cypher].nil? || session[:cypher]&.zero?
    end

    after '/sign_in' do
      blocked?(current_user.first) if current_user
    end

    get '/' do
      session[:block] = 0
      erb :welcome
    end

    get '/access' do
      erb :access
    end

    post '/access' do
      if params[:password] == KEY && session[:crypto] != 0
        decrypt('password.bin', KEY)
        redirect to '/'
      end

      redirect to '/access'
    end

    get '/about' do
      erb :about
    end

    get '/blocked' do
      erb :'users/blocked'
    end

    get '/sign_in' do
      redirect to '/admin' if current_user

      erb :sign_in
    end

    post '/sign_in' do
      user = Auth.db[:users].where(login: params[:login])
      user_hash = user.first
      if user_hash && check_password(params[:password], user_hash[:password])
        session.clear
        session[:token] = user_hash[:token]
        session[:block] = 0
        if admin?
          redirect to '/admin'
        else
          redirect to "/users/#{user_hash[:id]}"
        end
      else
        check_session_block
        session[:block] = session[:block].nil? ? 0 : session[:block] + 1
        @error = 'Username or password was incorrect'
        erb :sign_in
      end
    end

    post '/sign_out' do
      @error = ''
      @completed = ''
      session.clear
      redirect to '/'
    end

    post '/exit' do
      encrypt('password.bin', KEY)
      exit
    end

    namespace '/admin' do
      before do
        redirect_if_not_logged_in
        redirect to "/users/#{current_user.first[:id]}" unless admin?
      end

      get  { erb :'admin/show' }

      get '/user_list' do
        puts "error: #{@error}"
        @users = Auth.db[:users].all
        erb :'admin/list'
      end

      post '/create_user' do
        Auth.db[:users].insert(name: params[:login],
                               login: params[:login],
                               password: hash_password(''),
                               token: SecureRandom.hex(10))
      rescue Sequel::UniqueConstraintViolation
        @error = 'Username already taken'
      ensure
        redirect to '/admin/user_list'
      end

      post '/update_user' do
        block = params[:block] == 'block'
        limitation = params[:limitation] == 'limitation'
        Auth.db[:users].where(login: params[:login])
            .exclude(role: 'admin')
            .update(block: block, limitation: limitation)

        redirect to '/admin/user_list'
      end

      post '/change_user_password' do
        new_password = hash_password(params[:password])
        user = Auth.db[:users].where(id: params[:id])
        user.update(password: new_password)
        puts 'User password updated!'

        redirect to '/admin'
      end
    end

    namespace '/users' do
      before do
        redirect_if_not_logged_in
      end

      get '/:id' do
        @id = current_user.first[:id]
        erb :'users/show'
      end

      post '/:id/change_password' do
        if limited? && !check_limitation_pass(params[:password])
          @completed = ''
          @error = "Password doesn't match fot limitation"
        else
          change_password(current_user,
                          params[:old_password],
                          params[:password])
          puts 'User password updated!'
          @error = ''
          @completed = 'Password updated'
        end

        erb :'users/show'
      end
    end

    not_found do
      erb :not_found
    end

    run! if __FILE__ == $PROGRAM_NAME
  end
  # rubocop:enable Metrics/ClassLength
  # rubocop:enable Metrics/BlockLength
end
